# PetStore Api definition

This is an example how to document API Definition using REDOC CLI and YAMLINK

## Local setup

Install redoc

```npm install -g redoc-cli```

Install yamlinc

```npm install -g yamlinc```

Generate your index.yml

Local preview run:

 1. Join all the yml files with :```yamlinc index.yml```
 2. Serve locally your changes on localhost ```redoc-cli serve index.inc.yml --watch ```
 3. Generate your HTML ready for publish ```redoc-cli bundle index.inc.yml -o public/index.html```
